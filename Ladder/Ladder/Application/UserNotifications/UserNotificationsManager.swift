//
//  UserNotificationsManager.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 29.10.2021.
//

import Foundation
import UserNotifications
import Firebase
import FirebaseMessaging

class UserNotificationManager: NSObject, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    static let sharedInstance = UserNotificationManager()
    private override init() {}
    
    func remoteUserNotification(_ application: UIApplication) {
        //конфиг Firebase messaging
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        //запрос на регистрацию получения PushNotifications
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (sucsess, error) in
            print("Разрешение на отправку полученных уведомлений: \(sucsess)")
            guard sucsess else { return }
            DispatchQueue.main.async {
                application.registerForRemoteNotifications()
            }
        }
    }
}
