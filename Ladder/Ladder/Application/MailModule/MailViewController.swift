//
//  MailViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 13.10.2021.
//

import UIKit
import MessageUI

class MailViewController: UIViewController, MFMailComposeViewControllerDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //отправки сообщений
    @IBAction func  mailSend(_ sender: Any) {
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }
    }
    
    //конфигурация сообщений в тч адрес отправки
    func configureMailController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["ladder@denvich.com"])
        return mailComposerVC
    }
    
    //сообщение о ошибках отправки сообщения при их возникновении
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Не возможно отправить сообщение", message: "Подключите учетную запись в Почте или попробуйте позже", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    //проверки результата отправки сообщения
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
