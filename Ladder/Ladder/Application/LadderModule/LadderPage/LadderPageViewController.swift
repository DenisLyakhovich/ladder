//
//  LadderIconViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 01.11.2021.
//

import UIKit

class LadderPageViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var ladderPageScrollView: UIScrollView!
    @IBOutlet weak var ladderPageControl: UIPageControl!
    
    let ladderPageView = ["LadderPage1","LadderPage2","LadderPage3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPageView()
    }
    
    //создание страниц
    func createPageView() {
        ladderPageControl.numberOfPages = ladderPageView.count
        for i in 0..<ladderPageView.count{
            let imageLadder = UIImageView()
            imageLadder.contentMode = .scaleAspectFill
            imageLadder.image = UIImage(named: ladderPageView[i])
            let xPos = CGFloat(i)*self.view.bounds.size.width
            imageLadder.frame = CGRect(x: xPos, y: 0, width: view.frame.size.width, height: ladderPageScrollView.frame.size.height)
            ladderPageScrollView.contentSize.width = view.frame.size.width*CGFloat(i+1)
            ladderPageScrollView.addSubview(imageLadder)
        }
    }
    
    //прокручивание страниц
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = ladderPageScrollView.contentOffset.x/ladderPageScrollView.frame.width
        ladderPageControl.currentPage = Int(page)
    }
}
