//
//  LadderDetailViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 18.10.2021.
//

import UIKit

class LadderDetailViewController: UIViewController, UITextViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var textView: UITextView!
    
    var contentTextView = ""
    var contentHeader = ""
    var contentNumber = ""
    var contentStep = ""
 
    override func viewDidLoad() {
        super.viewDidLoad()
        dataDisplay()
    }
    
    //отображение получаемых данных в UITextView
    func dataDisplay() {
        textView.text = contentTextView
        navigationItem.title = contentHeader
        navigationItem.prompt = "\(contentStep) \(contentNumber)"
    }
}
