//
//  LadderTableViewCell.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 18.10.2021.
//

import UIKit

class LadderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var stepView: UIView!
    @IBOutlet weak var headerLable: UILabel!
    @IBOutlet weak var numberLable: UILabel!
    @IBOutlet weak var stepLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        numberView.layer.cornerRadius = 10
        stepView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //вывод данных в UI ячейки
    func  castomizeCell(ladderData: Ladder) {
        headerLable.text = ladderData.header
        numberLable.text = ladderData.number
        stepLabel.text = ladderData.step
    }
}
