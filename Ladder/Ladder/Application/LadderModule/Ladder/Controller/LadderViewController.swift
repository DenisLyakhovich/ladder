//
//  LadderViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 18.10.2021.
//

import UIKit
import CoreData
import Foundation

class LadderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    //свойство для работы с данными получаемымы из сети по API
    var contents: [LadderDataModel] = []
    //свойство для работы с данными Core Data
    var ladders: [Ladder] = []
    //свойство идентификатора ячейки для подключения в таблицу
    var reuseIdentifire = "ladder cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "LadderTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifire)
        tableView.backgroundColor = UIColor.clear
        loadDataApi()
        loadCoreDataObject()
        deleteCoreDataObject()
    }
    
    //определяет количество ячеек
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ladders.count
    }
    
    //отображает содержимое ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as! LadderTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.castomizeCell(ladderData: ladders[indexPath .row])
        return cell
    }
    
    //определяет высоту ячейки
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //отображение таблицы
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // установки separator tableView - длина-высота-цвет
        let additionalSeparatorThickness = CGFloat(2)
        let additionalSeparator = UIView(frame: CGRect(x: 80, y: cell.frame.size.height, width: 315, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.black
        cell.addSubview(additionalSeparator)
    }
    
    //навигаця и предача данных в LadderDetailViewController
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = ladders[indexPath.row]
        let goToVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LadderDetailViewController") as? LadderDetailViewController
        goToVc?.contentTextView = item.content!
        goToVc?.contentHeader = item.header!
        goToVc?.contentNumber = item.number!
        goToVc?.contentStep = item.step!
        goToVc?.navigationItem.title = item.header
        navigationController?.pushViewController(goToVc!, animated: true)
    }
}
