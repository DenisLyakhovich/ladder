//
//  LadderCoreDataExtension.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 30.10.2021.
//

import Foundation
import CoreData

extension LadderViewController {
    //создание объекта Core Data и его сохрание в БД
    func createCoreDataObject(cont: [LadderDataModel]) {
        //буферный массив для корректного отображения данных с учетом работы функции deleteAllData()
        var buffer: [Ladder] = []
            for ladder in cont {
                let ladderItem = Ladder(context: CoreDataManager.sharedInstance.mainContext)
                ladderItem.id = ladder.id
                ladderItem.number = ladder.number
                ladderItem.header = ladder.header
                ladderItem.step = ladder.step
                ladderItem.content = ladder.content
                buffer.append(ladderItem)
            }
        ladders = buffer
            CoreDataManager.sharedInstance.saveContext()
    }
    
    //запрос на загрузку данных из Core Data
    func loadCoreDataObject() {
        let ladderRequest: NSFetchRequest<Ladder> = Ladder.fetchRequest()
        //сортировка данных по id
        ladderRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        do {
            ladders = try CoreDataManager.sharedInstance.mainContext.fetch(ladderRequest)
        } catch {
            print("Coud't load core data\(error.localizedDescription)")
        }
    }
    
    //удаление создаваемых объектов Core Data при их повторном создании с учетом работы функции loadData() (для того чтобы количество объектов Core Data не увеличивалось при получении данных по API)
    func deleteCoreDataObject() {
        let ladderRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Ladder")
        ladderRequest.returnsObjectsAsFaults = true
    do {
        let results = try CoreDataManager.sharedInstance.mainContext.fetch(ladderRequest)
        for managedObject in results {
            let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
            CoreDataManager.sharedInstance.mainContext.delete(managedObjectData)
        }} catch let error as NSError {
        print("Delete all data in entity error : \(error) \(error.userInfo)")
        }
    }
}
    
