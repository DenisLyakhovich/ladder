//
//  LadderApiService.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 19.10.2021.
//

import UIKit
import Foundation

extension LadderViewController {
    //сетевой запрос на сервер для получение данных по API
    func loadDataApi() {
        //свойство для работы с host аресом
        let stringURL = "http://denvich.com/api/ladder/ladderdatamodel.json"
        //инициализируем константу URL, c проверкой на возврат через guard
        guard let url = URL(string: stringURL) else { return }
        //формируем задачу для получения данных их сети
        let task = URLSession.shared.dataTask(with: url) { [self]
            //комплишн замыкание возвраающее ответ(response) от сервера в виде данных(data) или ошибки(error)
            (data, response, error) in
            //обработка ошибок error
            if let error = error {
                print(error.localizedDescription)
                return
            }
            //обработка ошибок response
            if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<300:
                    print("Status code: \(response.statusCode)")
                    break
                default:
                    print("Status code: \(response.statusCode)")
                }
            }
            //константа для работы с данными c проверкой на возврат через guard
            guard let data = data else { return }
            //парсинг данных - декодирование полученных даных из JSON в объкт data с проверкой guard на наличие данных, в случае отсутвия даннных в консоль выводится сообщение
            guard let jsonData = try? JSONDecoder().decode([LadderDataModel].self, from: data) else {
                print("Error - Can't parse JSON DataNetwork")
                return
            }
            dump(jsonData)
            self.contents = jsonData
            self.createCoreDataObject(cont: contents)
            self.updateTableView()
        }
        //вызов задачи
        task.resume()    
    }

    //обновлени данных, перезагрузка TableView
    func updateTableView(){
        DispatchQueue.main.async {
            //перегрузка таблицы для отображения данных полученных из сети по API
            self.tableView.reloadData()
        }
    }
}


