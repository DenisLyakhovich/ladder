//
//  LadderDataModel.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 19.10.2021.
//

import Foundation

struct LadderDataModel: Decodable {
    let id: Int64
    let number: String
    let header: String
    let step: String
    let content: String
}
