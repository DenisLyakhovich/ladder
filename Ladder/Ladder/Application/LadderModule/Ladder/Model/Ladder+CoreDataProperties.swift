//
//  Ladder+CoreDataProperties.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 28.10.2021.
//
//

import Foundation
import CoreData

extension Ladder {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Ladder> {
        return NSFetchRequest<Ladder>(entityName: "Ladder")
    }
    @NSManaged public var content: String?
    @NSManaged public var header: String?
    @NSManaged public var number: String?
    @NSManaged public var step: String?
    @NSManaged public var id: Int64
}

extension Ladder : Identifiable {

}
