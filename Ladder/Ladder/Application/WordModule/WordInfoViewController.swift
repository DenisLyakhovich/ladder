//
//  WordInfoViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 02.11.2021.
//

import UIKit

class WordInfoViewController: UIViewController {

    @IBOutlet weak var wordInfoButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wordInfoButton.tintColor = UIColor.white
    }

    @IBAction func clousePopOver(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
