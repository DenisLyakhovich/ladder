//
//  WordApiService.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 16.10.2021.
//

import UIKit
import Foundation

extension WordViewController {
    //сетевой запрос на сервер для получение данных по API
    func loadDataApi() {
        //свойство для работы с host аресом
        let stringURL = "http://denvich.com/api/ladder/worddatamodel.json"
        //инициализируем константу URL, c проверкой на возврат через guard
        guard let url = URL(string: stringURL) else { return }
        //формируем задачу для получения данных их сети
        let task = URLSession.shared.dataTask(with: url) {
            //комплишн замыкание возвраающее ответ(response) от сервера в виде данных(data) и ошибки(error)
            (data, response, error) in
            //обработка ошибок error
            if let error = error {
                DispatchQueue.main.async {
                    self.alertData()
                }
                print(error.localizedDescription)
                return
            }
            //обработка ошибок response
            if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<300:
                    print("Status code: \(response.statusCode)")
                    break
                case 400..<600:
                    //если ответ сервера с ошибкой выводится алерт
                    DispatchQueue.main.async {
                        self.alertData()
                    }
                    print("Status code: \(response.statusCode)")
                    break
                default:
                    print("Status code: \(response.statusCode)")
                }
            }
            //константа для работы с данными c проверкой на возврат через guard
            guard let data = data else { return }
            //парсинг данных - декодирование полученных даных из JSON в объкт data с проверкой guard на наличие данных, в случае отсутвия даннных в консоль выводится сообщение
            guard let jsonData = try? JSONDecoder().decode([WordDataModel].self, from: data) else {
                print("Error - Can't parse JSON DataNetwork")
                return
            }
            dump(jsonData)
            self.contents = jsonData
            self.updateTableView()
        }
        //вызов задачи
        task.resume()
    }
    
    //обновление даных таблицы по свайпу вниз
    func refreshData() {
        //определение цвета рефрешера
        refreshControl.tintColor = UIColor.white
        //обновление данных
        refreshControl.addTarget(nil, action: #selector (refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        refreshControl.endRefreshing()
    }
    
    //дополнительная функция-селектор по очистке данных
    @objc func refresh() {
        //очиста массива данных полученных по сети из API
        contents = []
        //повторный вызов сетевого запроса на сервер для получение данных по API
        loadDataApi()
    }

    //обновлени данных, перезагрузка TableView
    func updateTableView(){
        DispatchQueue.main.async {
            //перегрузка таблицы для отображения данных полученных из сети по API
            self.tableView.reloadData()
            //остановка рефрешера после свайпа и обновления данных по API
            self.refreshControl.endRefreshing()
        }
    }

    //уведомление пользователю в случае отсутствия данных
    func alertData() {
        if contents.count == 0 {
            let alert = UIAlertController(title: "Отсутствуют данные",message: "Сервер не отвечает или нет сети",preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",style: .default)  {
                [unowned self] action in
                self.refreshData()
            }
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
}

extension WordTableViewCell {
    //сетевой запрос на получение картинок с сервера
    func iconParseCell(iconImage: WordDataModel?) -> UIImage {
        if let icon = iconImage?.icon {
            let url = URL(string: icon)
            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                if  data == nil {
                    print("No image, connecting the network")
                } else {
                    DispatchQueue.main.async {
                        self.iconView.image = UIImage(data: data!)
                    }
                }
            }).resume()
        }
        return UIImage()
    }
    
}
