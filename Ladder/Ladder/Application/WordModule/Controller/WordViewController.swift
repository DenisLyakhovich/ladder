//
//  WordViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 14.10.2021.
//

import UIKit

class WordViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate {
  
    //свойство для работы с рефрешерем
    var refreshControl = UIRefreshControl()
    //свойство для работы с данными получаемымы из сети по API
    var contents: [WordDataModel] = []
    //свойство идентификатора ячейки для подключения в таблицу
    var reuseIdentifire = "word cell"
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //подключение кастомной ячейки WordTableViewCell
        tableView.register(UINib(nibName: "WordTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifire)
        //очистка фона, для отображения подключаемой картинки
        tableView.backgroundColor = UIColor.clear
        //назначение таб бар item возврата к верху таблицы
        tabBarController?.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        //вызов получения данных по API
        self.loadDataApi()
        //вызов обновления данных
        self.refreshData()
    }
    
    //возвращает количество строк в секций (обязательный метод который должен реализовывать протоколом UITableViewDataSource)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //возвращает количичество строк согласно получаемой модели данных WordDataModel по API
        return contents.count
    }
    
    //определяет ячейку (обязательный метод который должен реализовывать протоколом UITableViewDataSource)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //подключение кастомной ячейки WordTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as! WordTableViewCell
        //очистка фона ячейки, для установления ее прозрачности
        cell.backgroundColor = UIColor.clear
        //обновление данных ячейки из модели данных WordDataModel по API
        cell.castomizeCell(wordData: contents[indexPath.row])
        return cell
    }
    
    //определяет высоту ячейки
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 270
    }
    
    //отображение таблицы
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // установки separator tableView - длина-высота-цвет
        let additionalSeparatorThickness = CGFloat(2)
        let additionalSeparator = UIView(frame: CGRect(x: 100, y: cell.frame.size.height, width: 300, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.white
        cell.addSubview(additionalSeparator)
    }

    //переход к верху tableView по тапу на tabBarItem
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            self.tableView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
}
