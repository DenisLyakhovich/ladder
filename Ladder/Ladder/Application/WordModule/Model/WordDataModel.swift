//
//  WordDataModel.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 15.10.2021.
//

import Foundation

struct WordDataModel: Decodable {
    let word: String
    let quote: String
    let author: String
    let icon: String
}


