//
//  WordTableViewCell.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 14.10.2021.
//

import UIKit

class WordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var wordLaber: UILabel!
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //скругление углов у view
        mainView.layer.cornerRadius = 10
        subView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //вывод данных в UI ячейки
    func  castomizeCell(wordData: WordDataModel) {
        wordLaber.text = wordData.word
        quoteLabel.text = wordData.quote
        authorLabel.text = wordData.author
        iconView.image = self.iconParseCell(iconImage: wordData)
    }
}
