//
//  HeraldInfoViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 02.11.2021.
//

import UIKit

class HeraldInfoViewController: UIViewController {

    @IBOutlet weak var heraldInfoButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        heraldInfoButton.tintColor = UIColor.white
    }
    
    @IBAction func clousePopOver(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
