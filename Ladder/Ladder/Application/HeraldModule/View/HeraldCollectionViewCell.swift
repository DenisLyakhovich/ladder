//
//  HeraldCollectionViewCell.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 01.11.2021.
//

import UIKit

class HeraldCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var heraldImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //вывод данных в UI ячейки
    func  castomizeCell(heraldData: HeraldDataModel) {
        heraldImageView.image = self.parseImageCell(image: heraldData)
    }
}
