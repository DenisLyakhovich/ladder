//
//  HeraldDataModel.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 01.11.2021.
//

import Foundation

struct HeraldDataModel: Decodable {
    let page: String?
}
