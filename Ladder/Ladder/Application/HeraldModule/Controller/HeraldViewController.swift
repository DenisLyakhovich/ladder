//
//  HeraldCollViewController.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 01.11.2021.
//

import UIKit

class HeraldViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UITabBarControllerDelegate {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var imageContent: [HeraldDataModel] = []
    var reuseIdentifire = "herard cell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "HeraldCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifire)
        collectionView.backgroundColor = UIColor.clear
        let appearance = UITabBarItemAppearance()
        setTabBarItemBadgeAppearance(appearance)
        loadDataApi()
    }
    
    //определяем количество ячеек в секции коллекции
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageContent.count
    }
    
    //опредеяем ячейки коллекции
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifire, for: indexPath) as! HeraldCollectionViewCell
        cell.backgroundColor = UIColor.clear
        //отображение контрола при отображении коллекции
        pageControl.numberOfPages = imageContent.count
        cell.castomizeCell(heraldData: imageContent[indexPath.row])
        return cell
    }
    
    //определяем размеры ячейки в коллекции
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 414.0, height: 667.0)
    }
    
    //прокручиваем страницы коллеции c pagecontroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = collectionView.contentOffset.x/collectionView.frame.width
        pageControl.currentPage = Int(page)
    }
    
    //обновление данных
    @IBAction func refreshData(_ sender: Any) {
        //очистка модели данных
        imageContent = []
        //повторный запрос данных по API
        loadDataApi()
        //возврат-скрол к первому элементу коллекции
        self.collectionView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    private func setTabBarItemBadgeAppearance(_ itemAppearance: UITabBarItemAppearance) {
            //Adjust the badge position as well as set its color
            itemAppearance.normal.badgeBackgroundColor = .blue
            itemAppearance.normal.badgeTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            itemAppearance.normal.badgePositionAdjustment = UIOffset(horizontal: 10, vertical: -10)
        }
}
 
