//
//  HeraldCollApiExtension.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 01.11.2021.
//

import Foundation
import UIKit

extension HeraldViewController {
    //сетевой запрос на сервер для получение данных по API
    func loadDataApi() {
        //свойство для работы с host аресом
        let stringURL = "http://denvich.com/api/ladder/heralddatamodel.json"
        //инициализируем константу URL, c проверкой на возврат через guard
        guard let url = URL(string: stringURL) else { return }
        //формируем задачу для получения данных их сети
        let task = URLSession.shared.dataTask(with: url) {
            //комплишн замыкание возвраающее ответ(response) от сервера в виде данных(data) и ошибки(error)
            (data, response, error) in
            //обработка ошибок error
            if let error = error {
                DispatchQueue.main.async {
                    self.alertData()
                }
                print(error.localizedDescription)
                return
            }
            //обработка ошибок response
            if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<300:
                    print("Status code: \(response.statusCode)")
                    break
                case 400..<600:
                    //если ответ сервера с ошибкой выводится алерт
                    DispatchQueue.main.async {
                        self.alertData()
                    }
                    print("Status code: \(response.statusCode)")
                    break
                default:
                    print("Status code: \(response.statusCode)")
                }
            }
            //константа для работы с данными c проверкой на возврат через guard
            guard let data = data else { return }
            //парсинг данных - декодирование полученных даных из JSON в объкт data с проверкой guard на наличие данных, в случае отсутвия даннных в консоль выводится сообщение
            guard let jsonData = try? JSONDecoder().decode([HeraldDataModel].self, from: data) else {
                print("Error - Can't parse JSON DataNetwork")
                return
            }
            dump(jsonData)
            self.imageContent = jsonData
            self.updateCollectionView()
        }
        //вызов задачи
        task.resume()
    }
    
    //обновлени данных, перезагрузка CollectionView
    func updateCollectionView(){
        DispatchQueue.main.async {
            //перегрузка скроллвью для отображения данных полученных из сети по API
            self.collectionView.reloadData()
        }
    }
    
    //уведомление пользователю в случае отсутствия данных
    func alertData() {
        if imageContent.count == 0 {
            let alert = UIAlertController(title: "Отсутствуют данные",message: "Сервер не отвечает или нет сети",preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",style: .default)  
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
}

extension HeraldCollectionViewCell {
    //сетевой запрос на получение картинок с сервера
    func parseImageCell(image: HeraldDataModel?) -> UIImage {
        if let imageHerald = image?.page{
            let url = URL(string: imageHerald)
            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                if  data == nil {
                    print("No image, connecting the network")
                } else {
                    DispatchQueue.main.async {
                        self.heraldImageView.image = UIImage(data: data!)
                    }
                }
            }).resume()
        }
        return UIImage()
    }
}

