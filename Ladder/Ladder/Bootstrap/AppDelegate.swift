//
//  AppDelegate.swift
//  Ladder
//
//  Created by Denis Lyakhovich on 04.10.2021.
//
import Firebase
import FirebaseMessaging
import UserNotifications
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //задержка launchscreen
        Thread.sleep(forTimeInterval: 3.0)
        
        //подключаем директорию файла базы данных для отображения в консоли
        CoreDataManager.sharedInstance.applicationDocumentsDirectory()
        
        //удаленная отправка user notification FirebaseMessaging
        UserNotificationManager.sharedInstance.remoteUserNotification(application)
        
        //сброс count badge
        UIApplication.shared.applicationIconBadgeNumber = 0;
        
        return true
    }
    
    //получение токена для отправки пуш
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        messaging.token {token, _ in
            guard let token = token else { return }
            print("Token: \(token)")
        }
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
}

